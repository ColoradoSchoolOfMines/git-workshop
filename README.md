# Git Workshop

Presentations for the CSM Git Workshop.

### Contents of each sub-directory:
- why-git: stating the problems git was created to solve
- theory: what git does
- local-git: how to use git on a personal project
- remote: how to use git collaboratively
- visualization: how to see changes you've made, history, etc.