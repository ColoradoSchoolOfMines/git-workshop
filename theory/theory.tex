\documentclass{beamer}

\usepackage{nth}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{tikz}

\title{Intro to git}
\author{Mines ACM}
\date{September \nth{7}, 2019}

\usetheme{Malmoe}
\usecolortheme{lily}

\begin{document}
\maketitle

\begin{frame}{What is git?}
  \pause
  \begin{itemize}[<+->]
  \item a version control system
    \begin{itemize}[<+->]
    \item it keeps track of the history of changes you've made to your files
    \item allows you to travel to any point in the history and view an old
      version of a file~\footnote{where you are right now is called
        \texttt{HEAD}}
    \end{itemize}
  \item distributed
    \begin{itemize}
    \item the history of your files can be easily copied from one computer to
      another
    \item this makes git very good at keeping track of changes for a project
      with multiple authors
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Let's say you start working on a text file}
  \begin{center}
    \includegraphics[width=0.75\textwidth]{pics/empty.png}

    we begin with a blank directory
  \end{center}
  \framebreak{}

  \begin{center}
    \includegraphics[width=0.75\textwidth]{pics/just_doc.png}

    we create a file
  \end{center}
  \framebreak{}

  \begin{center}
    \includegraphics[width=0.75\textwidth]{pics/edit.png}

    we edit the file
  \end{center}
\end{frame}

\begin{frame}{Let's bring Git into the picture}
  \pause

  Here's what we've done:
  \begin{itemize}
  \item made a file
  \item put text in the file
  \end{itemize}

  \pause

  Here's what we want to do:
  \begin{itemize}
  \item start using Git to keep track of further changes done to the file
  \item this is what we'll do over the next set of slides
  \end{itemize}
\end{frame}

\begin{frame}{The repo}
  \begin{itemize}
  \item Before we can begin, we need to establish our current folder as a
    repository (repo)
  \item A repo is simply a folder that git is keeping track of
  \end{itemize}
\end{frame}

\begin{frame}{Creating the repo}
  \pause
  \begin{enumerate}
  \item The next presenter will go over exactly how to make the directory a repo
    \pause
  \item Once that's done, your directory will have a \texttt{.git} folder
  \end{enumerate}
  \begin{center}
    \includegraphics[width=\textwidth]{pics/bare.png}
  \end{center}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Now that our current directory is a Git repo, we can use use Git to keep
    track of the history in all of the files in the directory \pause
  \item Let's now go over how we'll use Git
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{A typical workflow}
  \includegraphics[width=\textwidth]{graphs/workflow/workflow.png}
  \begin{center}
    this is a typical workflow for updating git about the state of your files
  \end{center}
  \framebreak{}

  \includegraphics[width=\textwidth]{graphs/workflow/edit.png}
  \begin{center}
    opening up your text editor to change stuff
  \end{center}
  \framebreak{}

  \includegraphics[width=\textwidth]{graphs/workflow/stage.png}
  \begin{center}
    telling git about your changes
  \end{center}
  \framebreak{}

  \includegraphics[width=\textwidth]{graphs/workflow/commit.png}
  \begin{center}
    committing the changes to the repo's history
  \end{center}
\end{frame}

\begin{frame}{Commits}
  \begin{itemize}
  \item A commit keeps track of \pause
    \begin{itemize}
    \item what was changed \pause
    \item who made the change \pause
    \item when the change was made \pause
    \item why the change was made \pause
    \end{itemize}
  \item Commits can be ``checked out'' AKA they are points in time that you can
    jump to on command \pause
  \item ``Checking out'' a commit will set the contents of all the files
    in the directory to how they were when the commit was made \pause
  \item Commits are identified via their SHA1 hash
  \end{itemize}
\end{frame}

\begin{frame}{What a commit looks like}
  \includegraphics[width=\textwidth]{pics/first-commit.png}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Git represents the history of the repo as a graph (i.e.\ nodes and
    edges) \pause
  \item the nodes are commits \pause
  \item every time you commit, a new node is created in the repos's graph
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{graphs/progression/1.png}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{graphs/progression/2.png}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{graphs/progression/3.png}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{graphs/progression/4.png}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{graphs/progression/5.png}
\end{frame}

\begin{frame}{Time-traveling}
  \includegraphics[width=\textwidth]{graphs/progression/5-0.png}
  \begin{itemize}
  \item Any one of these commits can be checkout out (i.e.\ you can jump the
    repo to that specific point in time)
  \item For each commit, you can ask questions like when was this line of the
    file added? by whom? at what time? why?
  \end{itemize}
\end{frame}
\begin{frame}{Time-traveling}
  \includegraphics[width=\textwidth]{graphs/progression/5-5.png}

  \vspace{1cm}

  \includegraphics[width=\textwidth]{pics/edits/newest.png}
\end{frame}
\begin{frame}{Time-traveling}
  \includegraphics[width=\textwidth]{graphs/progression/5-1.png}

  \vspace{1cm}

  \includegraphics[width=\textwidth]{pics/edits/oldest.png}
\end{frame}
\begin{frame}{Time-traveling}
  \includegraphics[width=\textwidth]{graphs/progression/5-3.png}

  \vspace{1cm}

  \includegraphics[width=\textwidth]{pics/edits/newer.png}
\end{frame}
\begin{frame}{Time-traveling}
  \includegraphics[width=\textwidth]{graphs/progression/5-2.png}

  \vspace{1cm}

  \includegraphics[width=\textwidth]{pics/edits/older.png}
\end{frame}

\begin{frame}{Branches}
  \begin{itemize}
  \item having to remember the hashes of every commits would be painful \pause
  \item thus, Git has branches, which are pointers to commits that can also be
    checked out \pause
  \item think of them as giving names to commits \pause
  \item every repo you create will have a ``main'' or ``master'' branch by default \pause
  \end{itemize}

  \includegraphics[width=\textwidth]{graphs/dag/master.png}
\end{frame}

\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/1.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/2.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/3.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/4.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/5.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/6.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/7.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/8.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/9.png}
\end{frame}
\begin{frame}{Diverging}
  \includegraphics[width=\textwidth]{graphs/dag/diverge/10.png}
\end{frame}
\begin{frame}{Diverging}
  \begin{itemize}
  \item diverging branches are \textit{very} common in git repositories,
    particularly when you start collaborating with others
  \end{itemize}
\end{frame}

\begin{frame}{Merging}
  \begin{itemize}
  \item often times we want to apply the changes from one branch into another
    \pause
  \item this is done via merging
  \end{itemize}
  \includegraphics[width=\textwidth]{graphs/dag/merge/1.png}
\end{frame}
\begin{frame}{Merging}
  \begin{itemize}
  \item often times we want to apply the changes from one branch into another
  \item this is done via merging
  \end{itemize}
  \includegraphics[width=\textwidth]{graphs/dag/merge/2.png}
\end{frame}

\end{document}