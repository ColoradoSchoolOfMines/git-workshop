\documentclass{beamer}

\mode<presentation> {
\usetheme{Metropolis}
\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}
\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
\setbeamertemplate{footline}[page number] % To replace the footer line in all slides with a simple slide count uncomment this line
}

\usepackage{tcolorbox}
\usepackage{verbatim}

\newcommand{\T}{\texttt}

\title{Git for your local projects}
\author{Jordan Newport}
\institute{Mines ACM}

\begin{document}
\maketitle

\section{Setup}

\begin{frame}{The Terminal}
    Why are we having you use the terminal?
    \begin{itemize}
        \item GUIs are really just using the terminal behind the scenes
        \item The terminal is just another UI that you happen to type into
        \item Everything we have you do will work in a MacOS or Linux terminal
            or Git Bash on Windows. Not everything will work in every GUI
    \end{itemize}
\end{frame}

\begin{frame}{Your git repository}
    \begin{itemize}
        \item Your project will live in a \textbf{repository}.
        \item A repository is just a directory that contains the files being
            tracked by Git. You can use it just like any other directory.
        \item It also contains some metadata so Git can actually track things,
            but we don't really care about that so long as it exists.
    \end{itemize}
\end{frame}

\begin{frame}{Making a git repository}
    \begin{itemize}
        \item You can turn any directory into a git repository just by running:
            \tcbox{\T{git init}}
        \item This tells Git to start tracking your directory. It will:
            \begin{itemize}
                \item establish its metadata
                \item create a master branch with nothing on it
            \end{itemize}
        \item It will not actually start tracking any files yet, though.
    \end{itemize}
\end{frame}

\section{Add, Commit, and keeping track of status}

\begin{frame}{Tracking files}
    With Git, your files can be in a few different states:
    \begin{itemize}
        \item \textbf{Untracked}: Git doesn't know or care what happens to the
            file
        \item \textbf{Unstaged}: Git has noticed some changes to the file
        \item \textbf{Staged}: Git has noticed some changes to the file, and you
            have told Git that you want to commit these changes as a new node in
            the graph
        \item \textbf{Unchanged}: There are no changes to this file since the
            last commit
    \end{itemize}
    A file can also have some changes staged, and more changes that are
    unstaged.

    Other things you can do (renaming, deleting, etc.) are handled as special
    cases of changing the file
\end{frame} 

\begin{frame}{Seeing Git's current status}
    You can use the command
    \tcbox{\T{git status}}
    to see what Git is up to lately. It will show you:
    \begin{itemize}
        \item what branch you are on (covered later)
        \item which files are in which state
            \begin{itemize}
                \item If a file is unchanged, it will not be listed.
            \end{itemize}
        \item How to revert any changes you have made since your last commit
    \end{itemize}
    We will use it throughout the workshop to see if what we're doing is
    working.
\end{frame}

\begin{frame}{Seeing Git's history}
    You can use the command

    \tcbox{\T{git log}}

    to see what commits have happened. We will use it throughout the workshop to
    see if what we're doing is working.

    This command will be covered in much more depth later.
\end{frame}

\begin{frame}{Changing file state}
    \begin{itemize}
        \item You can 
            \begin{itemize}
                \item stage changes to already-tracked files
                \item start tracking files (and stage their current versions)
            \end{itemize}
            using the command

            \tcbox{\T{git add <files or directories you want to add>}}
        \item You can commit your changes as a new node using the command

            \tcbox{\T{git commit}}

            This will drop you in an editor (you should have configured this
            previously) where you can write a commit message. Save and exit the
            editor to finish the commit. More on this next slide.
    \end{itemize}
\end{frame} 

\begin{frame}{More on committing}
    \begin{itemize}
        \item Before you make your first commit, you will likely have to make a
            couple of entries in your git config, since commits keep track of
            who made them:

            \begin{tcolorbox}[upperbox=visible]
                \T{git config --global user.name "Your Name"}
                \tcblower
                \T{git config --global user.email "yourname@mines.edu"}
            \end{tcolorbox}

        \item If you want to just write a message, use

            \tcbox{\T{git commit -m "your message here"}}
        \item There are many schools of thought on writing good commit messages.
            Generally, you want to keep them to about 50 characters or less and
            describe what change the commit is making.
    \end{itemize}
\end{frame}

\begin{frame}{Representing Commits}
    There are two ways to describe a commit:
    \begin{itemize}
        \item Its hash. Each commit has a unique hash, e.g.
            \T{23b4f5d539c2068e017ef3be324d9be4f577363d}. You can refer to a
            commit by its full hash or by the first seven characters.
        \item Its offset from HEAD or a branch tip. Each commit can be
            referenced based on how far before HEAD or its branch tip it
            happened. HEAD is itself a commit, but before that there are
            HEAD\textasciitilde 1, HEAD\textasciitilde 2, and so on.
    \end{itemize}
\end{frame}

\begin{frame}{Knowing what is staged to commit}
    You may want to actually know how your files have been changed before you
    add. If so, you can run
    \tcbox{\T{git diff}}
    which will give you a nice list of what is being added to/removed from each
    file.

    If you have already added but have not committed yet, you can add
    \T{--staged} to the end.

    Tip: if there is a lot of output, it may drop you into the pager \T{less}.
    You can scroll using the arrow keys and exit using \T{q}.
\end{frame}

\begin{frame}{Practice time}
    It's time to practice your newfound Git skills before I cover the second
    half of local Git. What you should do:
    \begin{itemize}
        \item Make a new git repository:
            \begin{itemize}
                \item \T{mkdir repo-name}
                \item \T{cd repo-name}
                \item \T{git init}
            \end{itemize}
        \item Make one commit where you create a file containing the words
            ``Hello World''
        \item Make another commit where you replace ``Hello'' with ``Goodbye''.
        \item Play around with Git for a few minutes. Make sure you understand
            add/commit and the status commands. We're here to help with any
            questions you may have.
    \end{itemize}
\end{frame}

\section{Revert and Reset}
\begin{frame}{Fixing your Git mistakes}
    \begin{itemize}
        \item If you want to make a commit undoing a previous commit, you can
            use
            \tcbox{\T{git revert <hash of commit you want to revert>}}
    \end{itemize}
    \begin{itemize}
        \item If you want something to never have happened, you can use \T{git
                reset} in three different ways:
            \begin{itemize}
                \item \T{git reset --soft <commit hash>} to reset your HEAD
                    pointer to a commit but keep your staged files staged
                \item \T{git reset <commit hash>} to reset your HEAD pointer to a
                    commit and unstage (but keep) changed files
                \item \T{git reset --hard <commit hash>} to reset your HEAD
                    pointer to a commit and discard all of your changes
            \end{itemize}
    \end{itemize}
    See ohshitgit.com (or its less profane cousin dangitgit.com) for more help
    fixing your git mistakes.
\end{frame}

\begin{frame}{Practice Time 2}
    It's time to practice reverting and resetting. This isn't crucial, so we
    aren't going to spend too long, but let's take a couple of minutes.
    \begin{itemize}
        \item First, we're going to revert our commit that changed Hello to
            Goodbye
        \item Next, we're going to decide that that's actually a lot of extra
            commits to undo one change, so we're going to reset all the way back
            to the original state
    \end{itemize}
\end{frame}

\section{Branches}
% speaker note: doing 2 different things, or having 2 different people

\begin{frame}{Branches}
    A branch is simply a chain of commits that may go back to a shared ancestor
    with another branch at some point in the past.

    By default you have one branch, \T{master}.

    You can create more branches using the command
    \tcbox{\T{git checkout -b <new branch name>}}
    You can switch branches using the command
    \tcbox{\T{git checkout <branch to switch to>}}
\end{frame}
\begin{frame}{Seeing info on branches}
    You can see which branches you have and which one you're currently on using
    the command
    \tcbox{\T{git branch}}
    \T{git status} can also tell you which branch you are currently on.
\end{frame}

\begin{frame}{More on \T{git checkout}}
    \T{git checkout} can also check out a commit:
    \tcbox{\T{git checkout <commit hash>}}
    \begin{itemize}
        \item This will put you in ``detached HEAD'' state
        \item You can interact with files...
        \item But bad things will happen if you commit or use \T{git checkout}
            again after making changes
        \item prevent this (``reattach your HEAD'') by making yourself a new
            branch to be on
    \end{itemize}
\end{frame}

\begin{frame}{Merging branches}
    \begin{itemize}
        \item You probably want to be able to combine different branches at some
            point: use \T{git merge}
        \item looks at two branches and combines the changes made to both of
            them since their last common ancestor
        \item Generates a ``merge commit'' if necessary
    \end{itemize}
    You want to be on the target branch, then run the command
    \tcbox{\T{git merge <branch you're merging>}}
    If you're lucky, Git will use the fast-forward or recursive strategies,
    which are automatic.

    If you're not lucky, you will have a merge conflict.
\end{frame}

\begin{frame}{Merge conflicts}
    Merge conflicts happen because there are two sets of changes to a file (or
    several files), and Git can't figure out how to combine them. For example:
    \begin{itemize}
        \item A line was added in the same place in the file
        \item A line was changed in two different ways
        \item etc.
    \end{itemize}
    Git status will tell you what files have conflicts and what you should do
    next.
\end{frame}

\begin{frame}[fragile]{Merge conflicts}
    Inside your files, this is how Git presents merge conflicts (example taken
    from \T{man git-merge}):
    \small
    \begin{verbatim}
Here are lines that are either unchanged from the common
ancestor, or cleanly resolved because only one side changed.
<<<<<<< yours:sample.txt
Conflict resolution is hard;
let's go shopping.
=======
Git makes conflict resolution easy.
>>>>>>> theirs:sample.txt
And here is another line that is cleanly resolved or unmodified.
    \end{verbatim}
    \normalsize
    Your job is to edit the file until it looks the way you want it to
    (typically by picking one of the sets of changes), then add and commit.
\end{frame}

\begin{frame}{Practice Time 3}
    It's time to practice branching.

    I would like you to:
    \begin{itemize}
        \item Create and commit a new file containing the words ``Git sucks at
            merging''
        \item Go back to your initial commit and create a new branch off of it
        \item In that new branch, create and commit a file of the same name as
            above, containing the words ``Git is pretty good at merging''
        \item Merge the new branch back into your master branch. Your call on
            how to deal with the merge conflict :)
    \end{itemize}
\end{frame}

\begin{frame}{One Last Thing: gitignore}
    What if you don't want a particular file or type of file to be visible to
    Git? Use a \textbf{.gitignore} file
    \begin{itemize}
        \item A list of ``shell glob'' patterns (* matches anything) that match
            files or directories you want Git to totally ignore
        \item You can put a .gitignore file in any subdirectory of your
            git repo, and it applies to everything in that subdirectory
    \end{itemize}
    For example:
    \begin{itemize}
        \item Ignore all object files for C/C++: \T{*.o}
        \item Ignore your private API key: \T{client\textunderscore secret.json}
        \item Ignore Python build artifacts: \T{\textunderscore \textunderscore
                pycache\textunderscore \textunderscore /}
    \end{itemize}
\end{frame}

\begin{frame}{More Information}
    That's all I have for you, but if you want more information...
    Resources:
    \begin{itemize}
        \item Run \T{man git-<command you want to look up>} to see the manual
            page
        \item Take a look at the free book \textit{Pro Git}: https://git-scm.com/book/en/v2
        \item Git cheat sheet: https://github.github.com/training-kit/downloads/github-git-cheat-sheet/
    \end{itemize}
    Topics (to make you feel like a Git wizard):
    \begin{itemize}
        \item \T{git stash}: run \T{man git-stash}
        \item \T{git rebase}: run \T{man git-rebase} or take a look at the
            excellent tutorial at https://git-rebase.io
        \item \T{git tag}: run \T{man git-tag}
    \end{itemize}
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
