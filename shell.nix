{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.graphviz
    pkgs.imagemagick

    # keep this line if you use bash
    pkgs.bashInteractive
  ];
}
